# Robust Natural Language Inference

## SNLI Benchmark

|               | cross entropy | accuracy |
|---------------|---------------|----------|
| roberta-large | 0.2746        | 0.9255   |
| electra-large | 0.2651        | 0.9169   |
| xlnet-large   | 0.2582        | 0.9275   |
