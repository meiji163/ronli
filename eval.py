import json
import torch
from tqdm import tqdm
from torch.utils.data import SequentialSampler, TensorDataset, DataLoader
import torch.nn.functional as F


NLI_LABELS = {"entailment":0, "neutral":1, "contradiction":2}

def process_data(data_dir: str, tokenizer, bs=64):
    ''' Process SNLI data into dataloader for 3-label classification'''
    snli_data = []
    with open(data_dir, mode='r',encoding='utf-8') as f:
        for _, line in enumerate(f):
            cur_item = json.loads(line)
            if cur_item["gold_label"] != '-':
                snli_data.append(cur_item)

    sent_pairs = [(ex["sentence1"], ex["sentence2"]) for ex in snli_data]
    targs = [ NLI_LABELS[ex["gold_label"]] for ex in snli_data ]
    tokens = tokenizer.batch_encode_plus(sent_pairs,
                    max_length=128,
                    return_token_type_ids=True,
                    truncation=True,
                    padding='max_length',
                    return_tensors='pt')
    
    onehot_targs = F.one_hot(torch.tensor(targs), num_classes=3).float()
    dataset = TensorDataset(
                        tokens["input_ids"].long(), 
                        tokens["token_type_ids"].long(), 
                        tokens["attention_mask"].long(),
                        onehot_targs)

    dataloader = DataLoader(dataset, 
                            sampler=SequentialSampler(dataset), 
                            batch_size=bs)
    return sent_pairs, targs, dataloader

def eval(model, dataloader):
    if torch.cuda.is_available():
        model.to("cuda")

    logits = []
    losses = []
    with torch.no_grad():
        for input_ids, token_type_ids, attn_mask, targ in tqdm(dataloader):
            if torch.cuda.is_available():
                input_ids = input_ids.cuda()
                token_type_ids = token_type_ids.cuda()
                attn_mask = attn_mask.cuda()
                targ = targ.cuda()

            out = model(
                    input_ids,
                    attention_mask=attn_mask,
                    token_type_ids=token_type_ids,
                    labels=targ)
            logits.append(out.logits.to("cpu"))
            losses.append(out.loss.to("cpu"))
            
    # compute the predictions and avg cross-entropy
    probs = F.softmax(torch.cat(logits), dim=1)
    preds = torch.argmax(probs, dim=1)
    
    return preds, losses
    
